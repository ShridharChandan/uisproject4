=begin
 This class represents the controller for the comments which 
 are shown in the view for the feed show operation. 
=end
class CommentsController < ApplicationController
  
  #We would need to perform devise based authentication to 
  #access this controller.
  before_filter :authenticate_user!
  
  # GET /comments
  # GET /comments.json
  def index
    @comments = Comment.all

    respond_to do |format|
      format.html # index.html.erb
      #format.json { render json: @comments }
    end
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
    @comment = Comment.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      #format.json { render json: @comment }
    end
  end

  # GET /comments/new
  # GET /comments/new.json
  def new
    @comment = Comment.new

    respond_to do |format|
      format.html # new.html.erb
      #format.json { render json: @comment }
    end
  end

  # GET /comments/1/edit
  def edit
    @comment = Comment.find(params[:id])
  end

  # POST /comments
  # POST /comments.json
 def create
   
  @feed = Feed.find(params[:feed_id])
    @comment = @feed.comments.build(params[:comment])
    @comment.userid=current_user.email
    respond_to do |format|
      if @comment.save
        format.html { redirect_to(@feed, :notice => 'Comment was successfully created.') }
        format.xml  { render :xml => @feed, :status => :created, :location => @feed }
      else
        format.html { redirect_to(@feed, :notice => 
        'Comment could not be saved. Please fill in all fields')}
        format.xml  { render :xml => @comment.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /comments/1
  # PUT /comments/1.json
  def update
    @comment = Comment.find(params[:id])
    @feed = @comment.feed
    
    respond_to do |format|
      #check if the user is authorized to make changes to the commment.
      #he is allowed to make changes if he created the comments. Otherwise
      #show the unauthorized page to him.
      unless is_authorized_user_for_comment()
        format.html  { render :file =>  "#{Rails.root}/public/422.html" }
      end
      
      if @comment.update_attributes(params[:comment])
        format.html { redirect_to(@feed, :notice => 'Comment was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @comment.errors, :status => :unprocessable_entity }
      end
    end
  end
  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment = Comment.find(params[:id])
    @feed = Feed.find(params[:feed_id])
    @comment.destroy

    respond_to do |format|
       #check if the user is authorized to make changes to the commment.
      #he is allowed to make changes if he created the comments. Otherwise
      #show the unauthorized page to him.
      unless is_authorized_user_for_comment()
        format.html  { render :file =>  "#{Rails.root}/public/422.html" }
      end
      
      if @comment.update_attributes(params[:comment])
        format.html { redirect_to(@feed, :notice => 'Comment was successfully deleted.') }
        format.xml  { head :ok }
      end
    end
  end
  
  #This method is used to check if user who created the comment
  #is the same as the logged in user. This gives us authorization 
  #for update, destroy methods. It requires the comment to be set 
  #in the @comment member variable of the class.
  def is_authorized_user_for_comment
    return current_user.email == @comment.userid
  end
   
end
