
xml.instruct! :xml, :version => "1.0" 
xml.rss :version => "2.0" do
  xml.channel do
    xml.title @feed_title
    xml.description "Feeds from Feedstore"
    
		unless @rss_feeds.nil?
	    	for feed_item in @rss_feeds
	    	  	xml.item do
			        xml.title 		feed_item.title
			        xml.description feed_item.summary
			        xml.pubDate 	feed_item.published
			        xml.link 		feed_item.url
			        xml.guid 		feed_item.entry_id
			     end
	    	end
	     end
  	end
end