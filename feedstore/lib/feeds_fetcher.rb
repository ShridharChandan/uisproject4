#
#  This class is used to obtain the feeds associated with urls
#  It is also optimized to fetch multiple feeds.
#
class FeedsFetcher
  
  #The HTTP timeout for fetching a single feed for a given url.
  FEED_FETCH_TIMEOUT = 30
  
  attr_accessor :all_feeds

    @threads = Array.new
    @all_feeds = Hash.new
    
    #Initialize array and hash
    def initialize
        @threads = Array.new
        
        @all_feeds = Hash.new 
    end
    
      
    # Fectches the feeds from the array of URLs
    # Input is array of String urls
    # Output is a Hash with key = url and value = feeds
    # Each value has title,summary and other params specified by rss protocol
    #
    # Todo : Convert ap statements to logger.
    def fetch_multiple_feeds(url)
              
        for feed_url in url
            
            #We create multiple threads, so that each HTTP request is
            #sent out simultaneously and we get the response as quickly as we can.
            ap @threads
            @threads << Thread.new(feed_url) { |rss_feed_url|
            
               feed_fetcher = SingleFeedFetcher.new
              
               feed = feed_fetcher.fetch_feed(rss_feed_url)
              
               unless feed.nil?
                
                  # If there was a HTTP Error code returned,
                  # then it is available as a Fixnum
                  if feed.class == Fixnum
                      ap "Recieved HTTP error code"+feeds.to_s
                  else
                      @all_feeds[rss_feed_url] = feed
                  end
                
               end
            
            }
              
             
        end
    
        #Wait for threads to finish
        @threads.each { |aThread|  aThread.join }
           
=begin
        
        #Example : How to loop over the feeds in Hash
       
        for item in @all_feeds.keys
              ap "Key  is "+item.to_s
              
              feeds = @all_feeds[item]
              
              # No feeds were returned either because the url didnt respond
              # or is not a rss url
              if feeds.nil?
                  next
              end
          
              # If there was a HTTP Error code returned,
              # then it is available as a Fixnum
              if feeds.class == Fixnum
                  ap "Recieved HTTP error code"+feeds.to_s
                  next
              end
          
              ap "Feed Source  is  "+item.to_s
        end
=end        
        return @all_feeds
    end

   #Fetches single feed 
   def fetch_single_feed(rss_feed_url)
          
      feed_fetcher = SingleFeedFetcher.new
      feed = feed_fetcher.fetch_feed(rss_feed_url)
      
      #Actual feeds are in feed.entries, but we also return metadata.
      return feed          
   end

=begin
 Variation class to only obtain feeds associated with a single url 
=end
  class SingleFeedFetcher
  
    def fetch_feed(feed_url)
        feed = Feedzirra::Feed.fetch_and_parse(feed_url, :timeout => FEED_FETCH_TIMEOUT)
        return feed
    end
    
  end

end