class RemoveTagFromFeed < ActiveRecord::Migration
  def up
    remove_column :feeds, :tag
  end

  def down
    add_column :feeds, :tag, :string
  end
end
