# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db => seed (or created alongside the db with db => setup).
#
# Examples => 
#
#   cities = City.create([{  => :name => 'Chicago' }, {  => :name => 'Copenhagen' }])
#   Mayor.create( => :name => 'Emanuel',  => city => cities.first)

# Read about fixtures at http => //api.rubyonrails.org/classes/ActiveRecord/Fixtures.html

#   cities = City.create([{  => :name => 'Chicago' }, {  => :name => 'Copenhagen' }])
#   Mayor.create( => :name => 'Emanuel',  => city => cities.first)

#Create the feeds first.
feeds = Feed.create( 
  [
    {
      :name => "ABC-news",
      :url => "http://feeds.abcnews.com/abcnews/topstories", 
      :userid => "default"
    },
    {
      :name => "CNN - Top Stories", 
      :url => "http://rss.cnn.com/rss/cnn_topstories.rss", 
      :userid => "default"
    },
    {
      :name => "Washington Post",
      :url => "http://feeds.washingtonpost.com/rss/politics",
      :userid => "default"
    },
    {
      :name => "Real Clear Politics",
      :url => "http://feeds.feedburner.com/realclearpolitics/qlMj",
      :userid => "default"
    },
    {
      :name => "Chicago Tribune",
      :url => "http://feeds.feedburner.com/chicagotribune/business",
      :userid => "default"
    },
    {
      :name => "ESPN",
      :url => "http://sports.espn.go.com/espn/rss/news",
      :userid => "default"
    },
    {
      :name => "Huffington Post",
      :url => "http://www.huffingtonpost.com/feeds/verticals/culture/index.xml",
      :userid => "default" 
    },
    {
      :name => "Engadget",
      :url => "http://www.engadget.com/rss.xml",
      :userid => "default"
    },
    {
      :name => "LA Times",
      :url => "http://feeds.latimes.com/latimes/travel/",
      :userid => "default"
    },
    {
      :name => "Time.com",
      :url => "http://feeds.feedburner.com/time/healthland",
      :userid => "default"
    },
    {
      :name => "Gaurdian.co.uk",
      :url => "http://feeds.guardian.co.uk/theguardian/lifeandstyle/rss",
      :userid => "default"
    },
    {
      :name => "Wired.com",
      :url => "http://www.wired.com/wiredscience/feed/",
      :userid => "default" 
    },
    {
      :name => "Dilbert",
      :url => "http://feed.dilbert.com/dilbert/most_popular",
      :userid => "default"
    }
  ]
)

#Add tags to the feeds.
Tag.create(:tagname => "Headlines", :feed_id => feeds[0].id)
Tag.create(:tagname => "Headlines", :feed_id => feeds[1].id)
Tag.create(:tagname => "Politics", :feed_id => feeds[2].id)
Tag.create(:tagname => "Politics", :feed_id => feeds[3].id)
Tag.create(:tagname => "Business", :feed_id => feeds[4].id)
Tag.create(:tagname => "Sports", :feed_id => feeds[5].id)
Tag.create(:tagname => "Culture", :feed_id => feeds[6].id)
Tag.create(:tagname => "Technology", :feed_id => feeds[7].id)
Tag.create(:tagname => "Travel", :feed_id => feeds[8].id)
Tag.create(:tagname => "Health", :feed_id => feeds[9].id)
Tag.create(:tagname => "Lifestyle", :feed_id => feeds[10].id)
Tag.create(:tagname => "Science", :feed_id => feeds[11].id)
Tag.create(:tagname => "Comics", :feed_id => feeds[12].id)

=begin
feed2 = Feed.create( 
  :name => "CNN - Top Stories", 
  :url => "http://rss.cnn.com/rss/cnn_topstories.rss", 
  :userid => "default").save
Tag.create(:tagname => "Headlines", :feed_id => feed2.first.id).save
  
feed3 = Feed.create( 
  :name => "Washington Post",
  :url => "http://feeds.washingtonpost.com/rss/politics",
  :userid => "default").save
Tag.create(:tagname => "Politics", :feed_id => feed3.first.id).save

feed4 = Feed.create( 
  :name => "NY Times - World",
  :url => "http://www.nytimes.com/roomfordebate/topics/politics.rss",
  :userid => "default" ).save
Tag.create(:tagname => "Politics", :feed_id => feed4.first.id).save

feed5 = Feed.create( 
  :name => "Chicago Tribune",
  :url => "http://feeds.feedburner.com/chicagotribune/business",
  :userid => "default" ).save
Tag.create(:tagname => "Business", :feed_id => feed5.first.id).save

  
feed6 = Feed.create(
  :name => "ESPN",
  :url => "http://sports.espn.go.com/espn/rss/news",
  :userid => "default" ).save
#Tag.create(:tagname => "Sports", :feed_id => feed6.id).save

feed7 = Feed.create( 
  :name => "Huffington Post",
  :url => "http://www.huffingtonpost.com/feeds/verticals/culture/index.xml",
  :userid => "default" ).save
#Tag.create(:tagname => "Culture", :feed_id => feed7.id).save
 
feed8 = Feed.create(
  :name => "Engadget",
  :url => "http://www.engadget.com/rss.xml",
  :userid => "default" ).save
#Tag.create(:tagname => "Technology", :feed_id => feed8.id).save
  
feed9 = Feed.create(
  :name => "LA Times",
  :url => "http://feeds.latimes.com/latimes/travel/",
  :userid => "default" ).save
#Tag.create(:tagname => "Travel", :feed_id => feed9.id).save
  
feed10 = Feed.create(
  :name => "Time.com",
  :url => "http://feeds.feedburner.com/time/healthland",
  :userid => "default").save
#Tag.create(:tagname => "Health", :feed_id => feed10.id).save
  
feed11 = Feed.create( 
  :name => "Gaurdian.co.uk",
  :url => "http://feeds.guardian.co.uk/theguardian/lifeandstyle/rss",
  :userid => "default" ).save
#Tag.create(:tagname => "Lifestyle", :feed_id => feed11.id).save
  
feed12 = Feed.create( 
  :name => "Wired.com",
  :url => "http://www.wired.com/wiredscience/feed/",
  :userid => "default" ).save
#Tag.create(:tagname => "Science", :feed_id => feed12.id).save

feed13 = Feed.create(
  :name => "Dilbert",
  :url => "http://feed.dilbert.com/dilbert/most_popular",
  :userid => "default" ).save
#Tag.create(:tagname => "Comics", :feed_id => feed13.id).save
=end