# Load the rails application
require File.expand_path('../application', __FILE__)

#Add all the custom required libraries here.
require 'uri_validator'
require 'rss_validator'
require 'awesome_print'

# Initialize the rails application
Feedstore::Application.initialize!
